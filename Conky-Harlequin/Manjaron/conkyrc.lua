--[[
#=====================================================================================
# CONKY HARLEQUIN
#
# Date    : 24/10/2018
# Author  : TheGreatYellow67
# Version : 1.0 Manjaron
# License : Distributed under the terms of GNU GPL version 2 or later
#=====================================================================================
# CONKY VERSION >= 1.10
#
# For commands in conky.config section:
# http://conky.sourceforge.net/config_settings.html
#
# For commands in conky.text section:
# http://conky.sourceforge.net/variables.html
#
# A PDF "Configuration Settings.pdf" with all variables is provided in .docs folder
#=====================================================================================
# FONTS
# To avoid copyright infringements you will have to download
# and install the fonts yourself sometimes.
#=====================================================================================
# GENERAL INFO ABOUT FONTS
# Go and look for a nice font on sites like http://www.dafont.com/
# Download and unzip - double click the font to install it (font-manager must be installed)
# No font-manager then put fonts in ~/.fonts
# Change the font name in the conky
# The name can be known with a command in the terminal: fc-list | grep "part of name"
# Change width and height of the conky according to font
# Reboot your system or fc-cache -fv in terminal
# Enjoy
#=====================================================================================
# FONTS FOR THIS CONKY
# Droid Sans / Droid Sans Mono
#======================================================================================
#
# Readme
#
# These conky scripts are based on Harmattan conky using only the God-Mode.
# These scripts are tested in debian stretch 9.5 (Conky version 1.10)
#
# How to use
#
# Edit one of the conkyrc.lua file that you can find in each color variant, then
# replace the variables template6, template7, template8 and template9.
#
# Open Weather Map API key
#
# to get an API Key, first you have to sign up here https://openweathermap.org/appid
#
# City ID
#
# You can find an up-to-date city id list here:
# http://bulk.openweathermap.org/sample/city.list.json.gz
#
# there is also a city.list.json in the .docs Conky-Harlequin folder
#
# after you have replaced those variables with your own, save.
#
# Credits
#
# 1. zagortenay333 for Harmattan Conky
# 2. aresgon for Workspace Indicator Conky
# 5. Erik Dubois conky scripts for this comment section
#======================================================================================
]]

conky.config = {

	-- [ Generic Settings ]

	background=true,					-- Forked to background.
	cpu_avg_samples = 2,				-- The number of samples to average for CPU monitoring.
	net_avg_samples = 2,				-- The number of samples to average for net data.
	diskio_avg_samples = 10,			-- The number of samples to average for DISK I/O monitoring.
	update_interval = 1,				-- Update interval.
	double_buffer = true,				-- Use the Xdbe extension? (eliminates flicker).
	no_buffers = true,					-- Subtract (file system) buffers from used memory?
	imlib_cache_size=10,				-- Imlib2 image cache size, in bytes.
	if_up_strictness = 'address',		-- How strict if testing interface is up - up, link or address
	temperature_unit = 'celsius',		-- Fahrenheit or celsius.

	-- [ Placement ]

	alignment = 'middle_right',			-- top_left,top_middle,top_right,bottom_left,bottom_middle,bottom_right,
										-- middle_left,middle_middle,middle_right,none.
	gap_x = 5,							-- Pixels between right or left border.
	gap_y = 0,							-- Pixels between bottom or left border.
	minimum_height = 900,				-- Minimum height of window.
	minimum_width = 240,				-- Minimum width of window.
	maximum_width = 240,				-- Maximum width of window.

	-- [ Graphical ]

	border_inner_margin = 0,			-- margin between border and text
	border_outer_margin = 0,			-- margin between border and edge of window
	border_width = 0,					-- Border width in pixels.
	draw_borders = false,				-- Draw borders around text.
	draw_shades = false,				-- Draw shades.
	draw_outline = false,				-- Draw outline.
	draw_graph_borders = false,			-- Draw borders around graphs.
	default_graph_height = 26,			-- Default is 25.
	default_graph_width = 80,			-- Default is 0 - full width.
	stippled_borders = 0,				-- Dashing the border.

	-- [ Windows ]

	own_window = true,					-- Create your own window to draw.
	own_window_type = 'desktop',		-- If own_window = true options are: normal/override/dock/desktop/panel
	own_window_hints = "undecorated,below,sticky,skip_taskbar,skip_pager",
										-- if own_window true - just hints
										-- own_window_type sets it
	own_window_class = 'Conky',			-- manually set the WM_CLASS name for use with xprop
	own_window_transparent = true,		-- if own_window_argb_visual is true sets background opacity 0%

	-- ( with own_window_transparent = false )

	--own_window_argb_visual = true,	-- Use ARGB - composite manager required.
	--own_window_colour = '#000000',	-- Set colour if own_window_transparent is false.
	--own_window_argb_value = 255,		-- Real transparency - composite manager required 0-255.

	-- [ Textual ]

	font="Droid Sans:size=8",			-- Font for complete conky unless in code defined.
	uppercase = false,					-- Uppercase or not.
	use_spacer = 'none',				-- Adds spaces around certain objects to align - default none.
	use_xft = true,						-- xft font - anti-aliased font.
	xftalpha = 1,						-- Alpha of the xft font - between 0-1.
	text_buffer_size = 2048,			-- Size of buffer for display of content of large variables (default=256).
	override_utf8_locale = true,		-- Force UTF8 requires xft.
	short_units = true,					-- Shorten units from KiB to k.
	top_name_width = 8,					-- Width for $top name value default 15.
	pad_percents=2,						-- Pad percentages to this many decimals (0 = no padding).
	extra_newline = false,				-- Extra newline at the end - for asesome's wiboxes.
	format_human_readable = true,		-- KiB, MiB rather then number of bytes.
	max_text_width = 0,					-- 0 will make sure line does not get broken if width too small.
	max_user_text = 16384,				-- Max text in conky default 16384.
	top_name_verbose = false,			-- If true, top name shows the full command line of  each  process

	--  [ Colours ]

	default_color="323232",
	color1="323232",
	color2="6C6C6C",
	color3="A0D468",
	color4="FFFFFF",
	color5="E76917",
	color6="888888",
	color7="AC92EC",
	color8="CC0000",
	color9="FFFFFF",

	-- [ Graphs Color Variants ]

	template2="E76917 E76917",

	-- [ Harlequin Root Path ]

	template3="~/.conky/Conky-Harlequin",

	-- [ Cache Path ]

	template4="~/.cache/harlequin-conky",

	-- [ Color Variant Path ]

	template5="~/.conky/Conky-Harlequin/Manjaron",

	-- [ API Key ]

	template6="",

	-- [ City ID ]

	template7="",

	-- [ Temp Unit ]

	-- Values: default, metric, imperial.
	template8="",

	-- [ Locale ]

	-- e.g. "es_ES.UTF-8". Leave empty for default.
	template9="",

}

conky.text = [[
# [ WEATHER DATA ]
#
${execi 300 ${template3}/get_weather ${template6} ${template7} ${template8} ${template9}}\
#
# [ IMAGES ]
#
${image ${template5}/pic/top-bg.png -p 0,0 -s 240x60}\
${image ${template5}/pic/border.png -p 0,60 -s 240x100}\
${image ${template5}/pic/fav-color.png -p 0,60 -s 240x90}\
${image ${template5}/pic/triangle.png -p 191,138 -s 12x12}\
${image ${template5}/pic/bg-1.png -p 0,160 -s 240x80}\
${image ${template5}/pic/bg-2.png -p 0,240 -s 240x245}\
${image ${template5}/pic/bg-3.png -p 0,485 -s 240x215}\
${image ${template5}/pic/bg-4.png -p 0,700 -s 240x15}\
${image ${template5}/pic/bottom-bg.png -p 0,715 -s 240x185}\
${image ${template5}/pic/separator-v.png -p 80,156 -s 1x76}\
${image ${template5}/pic/separator-v.png -p 160,156 -s 1x76}\
${image ${template5}/pic/separator-v.png -p 120,240 -s 1x68}\
${image ${template5}/pic/separator-h.png -p 5,235 -s 230x1}\
${image ${template5}/pic/separator-h.png -p 5,312 -s 230x1}\
${image ${template5}/pic/separator-h.png -p 5,484 -s 230x1}\
${image ${template5}/pic/separator-h.png -p 5,832 -s 230x1}\
${image ${template5}/pic/pressure.png -p 15,132 -s 16x16}\
${image ${template5}/pic/humidity.png -p 80,132 -s 16x16}\
${image ${template5}/pic/wind-flag.png -p 126,132 -s 16x16}\
#
# [ TIME AND DATE ]
#
${voffset 4}${font Droid Sans Mono:size=22}${alignc}${color1}${time %H:%M}${font}${color}
${voffset 4}${font Droid Sans:size=10}${alignc}${color1}${execi 300 LANG=${template9} LC_TIME=${template9} date +"%A, %B %-d"}${font}${color}
#
# [ MAIN TEMPERATURE AND MIN / MAX TEMPS ]
#
${goto 14}${voffset 8}${font Droid Sans:size=25}${color4}${execi 300 jq -r .main.temp ${template4}/weather.json | awk '{print int($1+0.5)}' # round num}${if_match "$template8" == "metric"} °C${else}${if_match "$template8" == "imperial"} °F${else}${if_match "$template8" == "default"} K${endif}${endif}${endif}
${goto 100}${voffset -55}${font Droid Sans:size=8}${color4}min: ${execi 300 jq -r .main.temp_min ${template4}/weather.json | awk '{print int($1+0.5)}' # round num}${if_match "$template8" == "metric"} °C${else}${if_match "$template8" == "imperial"} °F${else}${if_match "$template8" == "default"} K${endif}${endif}${endif}
${goto 100}${voffset 2}${font Droid Sans:size=8}${color4}max: ${execi 300 jq -r .main.temp_max ${template4}/weather.json | awk '{print int($1+0.5)}' # round num}${if_match "$template8" == "metric"} °C${else}${if_match "$template8" == "imperial"} °F${else}${if_match "$template8" == "default"} K${endif}${endif}${endif}${font}${color}
#
# [ ACTUAL WEATHER CONDITION ICON ]
#
${execi 300 cp -f ${template3}/.icons/#fff__64/$(jq .weather[0].id ${template4}/weather.json).png ${template4}/weather-0.png}${image ${template4}/weather-0.png -p 175,62 -s 40x40}\
#
# [ CITY ]
#
${goto 15}${voffset 3}${font Droid Sans:style=Bold:size=11}${color4}${execi 300 jq -r .name ${template4}/weather.json | sed "s|\<.|\U&|g"}${font}${color}
#
# [ WEATHER DESCRIPTION ]
#
${goto 15}${voffset 1}${font Droid Sans :size=10}${color4}(${execi 300 jq -r .weather[0].description ${template4}/weather.json | sed "s|\<.|\U&|g"})${font}${color}
#
# [ MAIN PRESSURE, HUMIDITY, WIND SPEED ]
#
${goto 34}${voffset 4}${color4}${execi 300 jq -r .main.pressure ${template4}/weather.json | awk '{print int($1+0.5)}' # round num} hPa\
${goto 98}${color4}${execi 300 jq -r .main.humidity ${template4}/weather.json | awk '{print int($1+0.5)}' # round num} %${color}\
${goto 148}${color4}${execi 300 jq -r .wind.speed ${template4}/weather.json | awk '{print int($1+0.5)}' # round num}${if_match "$template8" == "metric"} m/s${else}${if_match "$template8" == "default"} m/s${else}${if_match "$template8" == "imperial"} mi/h${endif}${endif}${endif}${color}
#
# [ 3 DAYS FORECAST NAMES ]
#
${voffset 12}${alignc 77}${color4}${execi 300 LANG=${template9} LC_TIME=${template9} date +%^a}${color}
${voffset -13}${alignc}${color4}${execi 300 LANG=${template9} LC_TIME=${template9} date -d +1day +%^a}${color}
${voffset -13}${alignc -77}${color4}${execi 300 LANG=${template9} LC_TIME=${template9} date -d +2day +%^a}${color}
#
# [ 3 DAYS FORECAST ICONS ]
#
${execi 300 cp -f ${template3}/.icons/#fff__32/$(${template3}/parse_weather 'first' '.weather[0].id' '0').png ${template4}/weather-1.png}${image ${template4}/weather-1.png -p 28,175 -s 32x32}\
${execi 300 cp -f ${template3}/.icons/#fff__32/$(${template3}/parse_weather 'first' '.weather[0].id' '1').png ${template4}/weather-2.png}${image ${template4}/weather-2.png -p 105,175 -s 32x32}\
${execi 300 cp -f ${template3}/.icons/#fff__32/$(${template3}/parse_weather 'first' '.weather[0].id' '2').png ${template4}/weather-3.png}${image ${template4}/weather-3.png -p 182,175 -s 32x32}
#
# [ 3 DAYS FORECAST TEMPERATURES MIN/MAX ]
#
${voffset 28}${alignc 77}${color4}${execi 300 ${template3}/parse_weather 'avg' '.main.temp_min' '0'}${if_match "$template8" == "metric"} °C${else}${if_match "$template8" == "imperial"} °F${else}${if_match "$template8" == "default"} K${endif}${endif}${endif}/${execi 300 ${template3}/parse_weather 'avg' '.main.temp_max' '0'}${if_match "$template8" == "metric"} °C${else}${if_match "$template8" == "imperial"} °F${else}${if_match "$template8" == "default"} K${endif}${endif}${endif}${color}
${voffset -14}${alignc}${color4}${execi 300 ${template3}/parse_weather 'avg' '.main.temp_min' '1'}${if_match "$template8" == "metric"} °C${else}${if_match "$template8" == "imperial"} °F${else}${if_match "$template8" == "default"} K${endif}${endif}${endif}/${execi 300 ${template3}/parse_weather 'avg' '.main.temp_max' '1'}${if_match "$template8" == "metric"} °C${else}${if_match "$template8" == "imperial"} °F${else}${if_match "$template8" == "default"} K${endif}${endif}${endif}${color}
${voffset -14}${alignc -77}${color4}${execi 300 ${template3}/parse_weather 'avg' '.main.temp_min' '2'}${if_match "$template8" == "metric"} °C${else}${if_match "$template8" == "imperial"} °F${else}${if_match "$template8" == "default"} K${endif}${endif}${endif}/${execi 300 ${template3}/parse_weather 'avg' '.main.temp_max' '2'}${if_match "$template8" == "metric"} °C${else}${if_match "$template8" == "imperial"} °F${else}${if_match "$template8" == "default"} K${endif}${endif}${endif}${color}
#
# [ NETWORK STATUS ]
#
${if_existing /proc/net/route eno1}\
${voffset 19}${goto 20}${color5}Up: ${color6}${upspeed eno1}${color5}${goto 140}Down: ${color6}${downspeed eno1}
${voffset 2}${goto 20}${upspeedgraph eno1 26,80 ${template2}}${goto 140}${downspeedgraph eno1 26,80 ${template2}}
${voffset -2}${goto 20}${color5}Sent: ${color6}${totalup eno1}${color5}${goto 140}Received: ${color6}${totaldown eno1}
${voffset 12}${goto 20}${color5}Connections in:${alignr 20}${color6}[${tcp_portmon 1 32767 count}]
${voffset 4}${goto 20}${color5}Connections out:${alignr 20}${color6}[${tcp_portmon 32768 61000 count}]
${voffset 4}${goto 20}${color5}Total connections:${alignr 20}${color6}[${tcp_portmon 1 65535 count}]
${voffset 4}${goto 20}${color5}Wired LAN device:${alignr 20}${color6}eno1
${voffset 4}${goto 20}${color5}Local IP:${alignr 20}${color6}${addr eno1}
${voffset 4}${goto 20}${color5}Public IP:${alignr 20}${color6}${execi 60 wget -O - -q icanhazip.com}${voffset 76}
${else}\
${if_existing /proc/net/route wlp5s0}\
${voffset 19}${goto 20}${color5}Up: ${color6}${upspeed wlp5s0}${color5}${goto 140}Down: ${color6}${downspeed wlp5s0}
${voffset 2}${goto 20}${upspeedgraph wlp5s0 26,80 ${template2}}${goto 140}${downspeedgraph wlp5s0 26,80 ${template2}}
${voffset -2}${goto 20}${color5}Sent: ${color6}${totalup wlp5s0}${color5}${goto 140}Received: ${color6}${totaldown wlp5s0}
${voffset 8}${goto 20}${color5}Connections in:${alignr 20}${color6}[${tcp_portmon 1 32767 count}]
${voffset 2}${goto 20}${color5}Connections out:${alignr 20}${color6}[${tcp_portmon 32768 61000 count}]
${voffset 2}${goto 20}${color5}Total connections:${alignr 20}${color6}[${tcp_portmon 1 65535 count}]
${voffset 2}${goto 20}${color5}WiFi Quality:${alignr 20}${color6}${wireless_link_qual_perc wlp5s0}%
${voffset 2}${goto 20}${color5}Interface:${alignr 20}${color6}${gw_iface}
${voffset 2}${goto 20}${color5}Bitrate:${alignr 20}${color6}${wireless_bitrate wlp5s0}
${voffset 2}${goto 20}${color5}WiFi Mode:${alignr 20}${color6}${wireless_mode wlp5s0}
${voffset 2}${goto 20}${color5}Essid:${alignr 20}${color6}${wireless_essid wlp5s0}
${voffset 2}${goto 20}${color5}Local IP:${alignr 20}${color6}${addr wlp5s0}
${voffset 2}${goto 20}${color5}Public IP:${alignr 20}${color6}${execi 60 wget -O - -q icanhazip.com}
${voffset 2}${goto 20}${color5}Mac addr:${alignr 20}${color6}${wireless_ap wlp5s0}
${else}\
${voffset 100}${goto 20}${color6}Network offline!${color}
${image ${template5}/pic/offline.png -p 105,321 -s 16x16}${voffset 134}
${endif}${endif}\
#
# [ SYSTEM INFOS ]
#
${voffset 10}${goto 20}${color5}OS:${alignr 20}${color6}${execi 10800 lsb_release -a | grep Description | cut -f 2}
${voffset 2}${goto 20}${color5}Kernel:${alignr 20}${color6}${kernel}
${voffset 2}${goto 20}${color5}Processes:${alignr 20}${color6}${processes} ($running_processes running)
${voffset 2}${goto 20}${color5}User@Hostname:${alignr 20}${color6}${execi 10 id -u -n}@$nodename
${voffset 2}${goto 20}${color5}Updates:${alignr 20}${color6}${execi 360 aptitude search "~U" | wc -l | tail} Packages
${voffset 2}${goto 20}${color5}Uptime:${alignr 20}${color6}${uptime_short}
${voffset 2}${goto 20}${color5}MB temp:${alignr 20}${color6}${execi 10 sensors | grep 'temp2' | cut -c16-22}
${voffset 2}${goto 20}${color5}Cpu: ${color6}${cpu cpu0}%${alignr 20}${color5}${cpubar 5,113}
${voffset 2}${goto 20}${color5}Mem: ${color6}${memperc}%${alignr 20}${color5}${membar 5,113}
${voffset 2}${goto 20}${color5}Swap: ${color6}${swapperc}%${alignr 20}${color5}${swapbar 5,113}
${voffset 2}${goto 20}${color5}Root: ${color6}${fs_used_perc /}%${alignr 20}${color5}${fs_bar 5,113 /}
${voffset 2}${goto 20}${color5}Home: ${color6}${fs_used_perc /home}%${alignr 20}${color5}${fs_bar 5,113 /home}
${voffset 1}${goto 20}${loadgraph 21,200 ${template2} -l}
#
# [ TOP PROCESSES ]
#
${voffset -1}${goto 30}${color4}Proc${color}
${voffset -13}${alignc}${color4}Mem%${color}
${voffset -13}${alignr 30}${color4}Cpu%${color}
#
${voffset 6}${goto 17}${color5}${top name 1}${color}
${voffset 2}${goto 17}${color5}${top name 2}${color}
${voffset 2}${goto 17}${color5}${top name 3}${color}
${voffset 2}${goto 17}${color5}${top name 4}${color}
${voffset 2}${goto 17}${color5}${top name 5}${color}
${voffset 2}${goto 17}${color5}${top name 6}${color}
${voffset 2}${goto 17}${color5}${top name 7}${color}
#
${voffset -102}${alignc}${color1}${top mem 1}%${color}
${voffset 2}${alignc}${color1}${top mem 2}%${color}
${voffset 2}${alignc}${color1}${top mem 3}%${color}
${voffset 2}${alignc}${color1}${top mem 4}%${color}
${voffset 2}${alignc}${color1}${top mem 5}%${color}
${voffset 2}${alignc}${color1}${top mem 6}%${color}
${voffset 2}${alignc}${color1}${top mem 7}%${color}
#
${voffset -102}${alignr 25}${color6}${top cpu 1}%${color}
${voffset 2}${alignr 25}${color6}${top cpu 2}%${color}
${voffset 2}${alignr 25}${color6}${top cpu 3}%${color}
${voffset 2}${alignr 25}${color6}${top cpu 4}%${color}
${voffset 2}${alignr 25}${color6}${top cpu 5}%${color}
${voffset 2}${alignr 25}${color6}${top cpu 6}%${color}
${voffset 2}${alignr 25}${color6}${top cpu 7}%${color}
#
# [ WORKSPACE INDICATOR ]
#
# color1 #323232 = active workspaces
# color7 #F6682A = inactive workspaces
#
${voffset 6}${color5}${goto 85}${font Droid Sans:size=25}${if_match ${desktop}==1}${color1}${endif}•${color5}${offset 10}${if_match ${desktop}==2}${color1}${endif}•${color5}${offset 10}${if_match ${desktop}==3}${color1}${endif}•${color5}${offset 10}${if_match ${desktop}==4}${color1}${endif}•${color5}${font}
#
# [ ABOUT CONKY HARLEQUIN ]
#
${voffset -5}${alignc}${color5}${font Droid Sans:style=bold:size=9}Conky Harlequin (Manjaron)${font}${color}
${voffset 2}${alignc}${color6}${font Droid Sans:size=8}by TheGreatYellow67 (v.1.0 - 2018)
]]
