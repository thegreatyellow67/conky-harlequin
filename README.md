### Conky Harlequin

Conky Harlequin is a collection of themes for [conky](https://github.com/brndnmtthws/conky) that are a tweak of [zagortenay333 / Harmattan](https://github.com/zagortenay333/Harmattan) (only God-Mode version) and powered by [OpenWeatherMap](http://openweathermap.org/).

---

* [Compatibility](#compatibility)
* [Installation](#installation)
* [Use](#use)
* [API-Key](#api-key)
* [City](#city)
* [Units](#units)
* [Language](#language)
* [Themes](#themes)
* [Network Section](#network-section)
* [Credits](#credits)
* [Changelog](#changelog)
* [Preview](#preview)

---

### Compatibility:

* The latest version 1.0 of these themes supports conky `1.10.x`.

* These scripts are tested in Debian stretch 9.5 (Conky version 1.10) with a monitor resolution of 1920 x 1080.

* From distro to distro some adjustments may be necessary.

---

### Installation:

* Install **conky**, **curl** and **jq**.

* Make sure you have the **Droid Sans** font installed (https://www.fontsquirrel.com/fonts/droid-sans).

* Download a compressed archive of this repository (clicking on the **Download** button) and decompress it in a temporary folder or clone it with the command:

	`git clone https://gitlab.com/thegreatyellow67/conky-harlequin.git`

* Register a private API key on [OpenWeatherMap](http://openweathermap.org/) to get weather data.

* [Find the ID of your city](http://bulk.openweathermap.org/sample/city.list.json.gz); it's a compressed file list, decompress and look for. Alternately, there is a city.list.json file in the **.docs** Harlequin folder.

* In a terminal run script **harlequin_setup** inside the temporary harlequin folder and follow the instructions **(first, remember to get the Api Key and City ID - see sections below)**.

---

### Use:

* With script **harlequin_demo** you can make a slideshow of all the harlequin themes, running in a terminal:

	`harlequin_demo [number_of_seconds]`

* With script **harlequin_start** you can run just only one theme at time, running in a terminal:

	`harlequin_start [color_theme_name]`

* The script **harlequin_start_random** chooses for you a random theme.

---

### API-Key

Register a private API key on [OpenWeatherMap](http://openweathermap.org/) to get weather data.

Use it in setup script **harlequin_setup**.
You can change later in each color theme the value of `template6` variable inside the `conkyrc.lua`file.

---

### City ID

[Find the ID of your city](http://bulk.openweathermap.org/sample/city.list.json.gz).
There is also a city.list.json in the .docs Conky-Harlequin folder where you can find your city ID.

Use it in setup script **harlequin_setup**.
You can change later in each color theme the value of `template7` variable inside the `conkyrc.lua`file.

---

### Units

The script setup **harlequin_setup** asks you to choice for the units value.
You can change later in each color theme the value of `template8` variable inside the `conkyrc.lua`file (values are: default, metric, imperial).

---

### Language

In script setup **harlequin_setup** if you leave "local language" empty, these conky themes use your default locale.
You can change later in each color theme the value of `template9` variable inside the `conkyrc.lua`file.

[See the list of supported languages](http://openweathermap.org/current#multi)

> **NOTE:**  
> This conky has some hardcoded text that won't get translated, but you can edit it manually.

---

### Themes:

| #      | Name                     | #      | Name                     | #      | Name                     |
| :----: | ------------------------ | :----: | ------------------------ | :----: | ------------------------ |
| **1**  | Brown-Card               | **11** | Elune                    | **21** | **New-Ubuntu** (*)       |
| **2**  | Button                   | **12** | Flatts                   | **22** | Nord                     |
| **3**  | Cards                    | **13** | Green-Meadow             | **23** | Numix                    |
| **4**  | Ciliora-Prima            | **14** | **Lemon-Look** (*)       | **24** | **Numix-v2** (*)         |
| **5**  | Ciliora-Prima-v2         | **15** | **Manjaron** (*)         | **25** | **Numix-v3** (*)         |
| **6**  | Ciliora-Secunda          | **16** | Metro                    | **26** | **Numix-v4** (*)         |
| **7**  | Ciliora-Secunda-v2       | **17** | **New-Arcolinux** (*)    | **27** | OMG-Ubuntu               |
| **8**  | Ciliora-Tertia           | **18** | New-Debian               | **28** | Orange-Juice             |
| **9**  | Ciliora-Tertia-v2        | **19** | **New-Manjaro** (*)      | **29** | **Transparent** (*)      |
| **10** | Elementary               | **20** | New-Minty                | **30** | **Transparent-Dark** (*) |

(*) New themes added 10/24/2018
 
---

### Network Section:

In case the network section shouldn't be displayed, you need to find your network device name (depending on your distro, but usually with command `sudo ifconfig -a`) and replace it manually in conkyrc.lua file.

> **NOTE:**  
>
> Example: if you have a **wireless device** `wlp2s0`, replace `wlp5s0` device in conky themes running in a terminal the following command:
>
>	`find $HOME/.conky/Conky-Harlequin -type f -name "conkyrc.lua" -exec sed -i'' -e "s/wlp5s0/wlp2s0/g" {} +`
>
> instead, if you have a **wired device** `eth0`, replace `eno1` device in conky themes running in a terminal the following command:
>
>	`find $HOME/.conky/Conky-Harlequin -type f -name "conkyrc.lua" -exec sed -i'' -e "s/eno1/eth0/g" {} +`

---

### Credits:

* **zagortenay333** for Harmattan Conky (https://github.com/zagortenay333/Harmattan)
* **aresgon** for Workspace Indicator Conky
* **Erik Dubois** for the conkyrc.lua file comment section (https://github.com/erikdubois)

---

### Changelog:

* [10/23/2018] - Initial version 1.0 
* [10/24/2018] - Added ten new themes: **Lemon-Look**, **Manjaron**, **New-Arcolinux**, **New-Manjaro**, **New-Ubuntu**, **Numix-v2**, **Numix-v3**, **Numix-v4**, **Transparent**, **Transparent-Dark**. 

---

### Preview:

<img src="https://i.imgur.com/G16IXSh.png" id="preview">
